# Eesti loogikute keel Estolog

Estolog on tõeväärtustega arvutamise keel, kus saab lisaks kontrollida võrdust, moodustada tingimusavaldisi ning defineerida muutujaid. Seda kõike saab teha imeilusas eesti keeles! Keele abstraktne süntaks on rahvusvaheline (ehk suhteliselt igav), aga küll saame eesti keele omapäraga tegeleda põhiosa ülesandes...

    x := 0;
    y := 1;
    a := (x JA y);
    b := (x VOI y);
    
    (KUI (x = y) SIIS a MUIDU b)
    
Üleval on üks programm estologi (konkreetses) süntaksis. Selle näidisprogrammi käivitamise tulemus on _true_.
Estolog oleks muidugi vastanud eesti keeles, aga kuna implementeerime Javas, siis saame tulemuseks Java tõeväärtustüüpi väärtus.

## AST
Estologi AST klassid paiknevad _proovieksam.ast_ paketis. Keele konstruktsioone esitavad järgmised _EstologNode_ alamklassid:

* _EstologLiteraal_, _EstologMuutuja_ — tõeväärtusliteraal ja muutuja;
* _EstologJa_, _EstologVoi_ — konjunktsiooni ja disjunktsiooni operaator;
* _EstologVordus_ — kahe avaldise võrdsuse kontrollimise operaator;
* _EstologKui_ — tingimusavaldis (_ternaarne operaator_);
* _EstologDef_ — muutuja definitsioon;
* _EstologProg_ — terviklik programm, mis koosneb _definitsioonidest_ ja _kehast_ ehk täpselt ühest avaldisest (mis pole definitsioon).

Klassis _EstologNode_ on staatilised abimeetodid, millega saab mugavamalt abstraktse süntaksipuu luua. Nende abil saab ülaloleva näidisprogrammile vastava süntaksipuu ise ehitada: 

    prog(
        kui(vordus(var("x"), var("y")), var("a"), var("b")),
    
        def("x", lit(false)),
        def("y", lit(true)),
        def("a", ja(var("x"), var("y"))),
        def("b", voi(var("x"), var("y")))
    )

### Alusosa: EstologEvaluator
Väärtustamisele kehtivad järgmised nõuded:

1. Binaarsed operaatorid jälgivad standardseid loogikareegleid.
1. Tingimusavaldis on samuti standardne: sõltuvalt tingimuse väärtusest tuleb kasutada ühe või teise haru avaldise väärtust.
1. Tingimusavaldise muidu-haru võib puududa (st on `null`). Kui sellise tingimusavaldise tingimus on väär, siis on tingimusavaldis tervikuna tõene. 
1. Programmi väärtuseks on selle keha (mitte-definitsioonist avaldise) väärtus.
1. Programmi definitsioonid tuleb väärtustada enne programmi avaldise väärtustamist, mis võib sisaldada viiteid defineeritud muutujatele.
1. Programmi definitsioonid tuleb teostada selles järjekorras nagu nad listis on, kusjuures iga definitsiooni avaldis võib sisaldada viiteid eelnevalt defineeritud muutujatele.
1. Esialgu pole defineeritud ühtegi muutujat. Defineerimata muutujate kasutamine peab viskama  _NoSuchElementException_-i.
1. Programmis on lubatud üht muutujat defineerida mitu korda, kusjuures muutuja väärtuseks on tema viimati defineeritud avaldise väärtus.
1. Võib eeldada, et väärtustatav programm sisaldab ainult ühte _EstologProg_ tippu, mis on kõige välimine, st ükski sisemine avaldis pole omakorda programm, millel on oma sisemised definitsioonid.

### Põhiosa: EstologAst

Estolog keele grammatika tuleb implementeerida failis _Estolog.g4_ ja parsepuust AST-i koostamine klassis _EstologAst_. All on natuke pikem näide süntaktiliselt korrektsest programmist. Mõned muutujad on defineerimata, aga see ei ole süntaksanalüsaatori mure.

    a := kala JA lind;   
    b := 0 = (kass VOI a NING kala); 
    c := KUI a = b SIIS 1 MUIDU b JA c;
    KUI a SIIS KUI c SIIS kala MUIDU hiir

Süntaksile kehtivad järgmised nõuded:

1. Literaaliks võib olla `1` või `0`, `1` esindab tõest väärtust ja `0` väära väärtust.
1. Muutuja koosneb ühest või rohkemast ladina suur- või väiketähest.
1. Binaarne operaator koosneb kahest avaldisest, millede vahel on operaator (`NING`, `VOI`, `JA`, `=`), operaatorid on vasakassotsiatiivsed v.a. `=`, mis ei ole üldse assotsiatiivne (s.t. `a = b = c` ei ole lubatud). Kõige madalama prioriteediga on `NING`, seejärel kasvavas järjekorras `VOI`, `JA`, `=`. Pane tähele, et `NING` on madalama prioriteediga kui `VOI`, aga loogiline tähendus on tal sama kui `JA`-l.
1. Tingimusavaldis algab võtmesõnaga `KUI`, millele järgneb avaldis, seejärel võtmesõna `SIIS`, millele omakorda järgneb avaldis. Seejärel võib tulla võtmesõna `MUIDU` ning avaldis. Tingimusavaldis on madalama prioriteediga kui binaarsed operaatorid. Mitmesuse korral on muidu-haru seotud kõige viimase KUI-lausega (täpselt nagu Java if-laused).
1. Avaldistes võib kasutada sulge, mis on kõige kõrgema prioriteediga.
1. Definitsioon koosneb muutujanimest, võtmesõnast `:=` ja avaldisest.
1. Programm koosneb definitsioonidest (mida võib olla ka null tükki) ja lõppavaldisest. Definitsioonid on omavahel ja lõppavaldisest eraldatud semikoolonitega (`;`).
1. Tühisümboleid (tühikud, tabulaatorid, reavahetused) tuleb ignoreerida.


### Lõviosa: EstologCompiler

Estolog keele CMa-sse kompilaator tuleb implementeerida klassis _EstologCompiler_.
Kompileeritud programmi käivitamise tulemus peab olema sama nagu siis, kui seda _EstologEvaluatoriga_ väärtustada, st kehtivad kõik ülaltoodud väärtustamisreeglid.
Tõeväärtused teisendame täisarvudeks _CMaUtils.bool2int_ abil. 

Kompileeritud programmidele kehtivad järgmised nõuded:

1. Programmi (st selle mitte-definitsioonist avaldise väärtus) peab täitmise lõpuks olema stacki pealmine element.
1. Stacki kasutus peab olema mõistlik:
    1. Defineeritud muutujate väärtused peavad paiknema stacki alguses defineerimise järjekorras; st esimesena defineeritud muutuja stacki aadressil 0, teisena defineeritud muutuja stacki aadressil 1 jne.
    1. Samanimelisele muutujale uuesti defineerimine peab väärtuse salvestama selle muutuja olemasolevale stacki aadressile, mitte kasutusele võtma uut.
    1. Programmi täitmise lõpuks ei tohi olla stackis muid üleliigseid väärtusi.
1. Programmi täitmise algul on stack tühi. Defineerimata muutujate kasutamine peab viskama _NoSuchElementExceptioni_ kompileerimise ajal.

Allpool oleva programmi kompileerimisel saadud CMa koodi täitmise lõpuks peaks olema stack `[0, 1, 0, 1, 1]`, kus esimesena on muutuja `x` väärtus, teisena muutuja `y` väärtus, kolmandana muutuja `a` väärtus, neljandana muutuja `b` väärtus ja viimasena kogu programmi väärtus.

    x := 0;
    y := 1;
    a := (x JA y);
    b := (x VOI y);
    
    (KUI (x = y) SIIS a MUIDU b)