package estolog;

import estolog.ast.*;
import estolog.ast.aatomid.EstologLiteraal;
import estolog.ast.aatomid.EstologMuutuja;
import estolog.ast.operaatorid.EstologJa;
import estolog.ast.operaatorid.EstologVoi;
import estolog.ast.operaatorid.EstologVordus;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class EstologEvaluator {

    public static boolean eval(EstologProg prog) {
        EstologAstVisitor<Boolean> visitor = new EstologAstVisitor<>() {
            private final Map<String, Boolean> env = new HashMap<>();

            @Override
            public Boolean visit(EstologLiteraal literaal) {
                return literaal.getValue();
            }

            @Override
            public Boolean visit(EstologMuutuja muutuja) {
                Boolean value = env.get(muutuja.getNimi());
                if (value == null) throw new NoSuchElementException("Undefined variable " + muutuja.getNimi());
                else return value;
            }

            @Override
            public Boolean visit(EstologJa ja) {
                return visit(ja.getLeftChild()) && visit(ja.getRightChild());
            }

            @Override
            public Boolean visit(EstologVoi voi) {
                return visit(voi.getLeftChild()) || visit(voi.getRightChild());
            }

            @Override
            public Boolean visit(EstologVordus vordus) {
                return visit(vordus.getLeftChild()) == visit(vordus.getRightChild());
            }

            @Override
            public Boolean visit(EstologKui kui) {
                if (visit(kui.getKuiAvaldis())) {
                    return visit(kui.getSiisAvaldis());
                } else if (kui.getMuiduAvaldis() != null) {
                    return visit(kui.getMuiduAvaldis());
                } else {
                    return true; // kui MUIDU avaldis on puudu, tagastame true
                }
            }

            @Override
            public Boolean visit(EstologDef def) {
                env.put(def.getNimi(), visit(def.getAvaldis()));
                return null;
            }

            @Override
            public Boolean visit(EstologProg prog) {
                for (EstologDef def : prog.getDefs()) visit(def);
                return visit(prog.getAvaldis());
            }
        };

        return visitor.visit(prog);
    }

    public static void main(String[] args) {
        EstologProg prog = EstologNode.prog(
                EstologNode.kui(EstologNode.vordus(EstologNode.var("x"), EstologNode.var("y")), EstologNode.var("a"), EstologNode.var("b")),

                EstologNode.def("x", EstologNode.lit(false)),
                EstologNode.def("y", EstologNode.lit(true)),
                EstologNode.def("a", EstologNode.ja(EstologNode.var("x"), EstologNode.var("y"))),
                EstologNode.def("b", EstologNode.voi(EstologNode.var("x"), EstologNode.var("y")))
        );

        System.out.println(prog);
        System.out.println(eval(prog));
    }
}
