package estolog.ast;

import toylangs.AbstractNode;

import java.util.List;

public class EstologProg extends EstologNode {
    private final List<EstologDef> defs;
    private final EstologNode avaldis;

    public EstologProg(List<EstologDef> defs, EstologNode avaldis) {
        this.defs = defs;
        this.avaldis = avaldis;
    }

    public List<EstologDef> getDefs() {
        return defs;
    }

    public EstologNode getAvaldis() {
        return avaldis;
    }

    @Override
    protected List<? extends AbstractNode> getAbstractNodeList() {
        return AbstractNode.cons(avaldis, defs);
    }

    @Override
    public <T> T accept(EstologAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
