package estolog.ast.operaatorid;

import estolog.ast.EstologAstVisitor;
import estolog.ast.EstologNode;

public class EstologVoi extends EstologBinOp {
    public EstologVoi(EstologNode leftChild, EstologNode rightChild) {
        super(leftChild, rightChild);
    }

    @Override
    protected String getOpName() {
        return "VOI";
    }

    @Override
    public <T> T accept(EstologAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
