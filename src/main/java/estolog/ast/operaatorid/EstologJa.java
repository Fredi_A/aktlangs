package estolog.ast.operaatorid;

import estolog.ast.EstologAstVisitor;
import estolog.ast.EstologNode;

public class EstologJa extends EstologBinOp {

    public EstologJa(EstologNode leftChild, EstologNode rightChild) {
        super(leftChild, rightChild);
    }

    @Override
    protected String getOpName() {
        return "JA";
    }

    @Override
    public <T> T accept(EstologAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
