package estolog.ast.operaatorid;

import estolog.ast.EstologAstVisitor;
import estolog.ast.EstologNode;

public class EstologVordus extends EstologBinOp {

    public EstologVordus(EstologNode leftChild, EstologNode rightChild) {
        super(leftChild, rightChild);
    }

    @Override
    protected String getOpName() {
        return "=";
    }

    @Override
    public <T> T accept(EstologAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
