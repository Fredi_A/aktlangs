package estolog;

import estolog.ast.EstologDef;
import estolog.ast.EstologNode;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import utils.ExceptionErrorListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static estolog.EstologParser.*;


public class EstologAst {

    public static void main(String[] args) throws IOException {
        EstologNode ast = makeEstologAst("x := 0;\n" +
                "y := 1;\n" +
                "a := (x JA y);\n" +
                "b := (x VOI y);\n" +
                "\n" +
                "(KUI (x = y) SIIS a MUIDU b)");
        System.out.println(ast);
        ast.renderPngFile("tree.png");
    }

    public static EstologNode makeEstologAst(String input) {
        EstologLexer lexer = new EstologLexer(CharStreams.fromString(input));
        lexer.removeErrorListeners();
        lexer.addErrorListener(new ExceptionErrorListener());

        EstologParser parser = new EstologParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.setErrorHandler(new BailErrorStrategy());

        ParseTree tree = parser.init();
        //System.out.println(tree.toStringTree(parser));
        return parseTreeToAst(tree);
    }

    // Implementeeri see meetod.
    private static EstologNode parseTreeToAst(ParseTree tree) {
        EstologBaseVisitor<EstologNode> visitor = new EstologBaseVisitor<>() {
            @Override
            public EstologNode visitInit(InitContext ctx) {
                return visit(ctx.prog());
            }

            @Override
            public EstologNode visitProg(ProgContext ctx) {
                List<EstologDef> defs = new ArrayList<>();
                for (DefContext defContext : ctx.def()) {
                    defs.add(visitDef(defContext)); // otse õige visit kutse
                }
                EstologNode avaldis = visit(ctx.avaldis());
                return EstologNode.prog(defs, avaldis);
            }

            @Override
            public EstologDef visitDef(DefContext ctx) {
                // täpsustatud tagastustüüp
                return EstologNode.def(ctx.Muutuja().getText(), visit(ctx.avaldis()));
            }

            @Override
            public EstologNode visitLiteraal(LiteraalContext ctx) {
                return EstologNode.lit(ctx.getText().equals("1"));
            }

            @Override
            public EstologNode visitMuutuja(MuutujaContext ctx) {
                return EstologNode.var(ctx.getText());
            }

            @Override
            public EstologNode visitSulud(SuludContext ctx) {
                return visit(ctx.avaldis());
            }

            @Override
            public EstologNode visitBinOp(BinOpContext ctx) {
                EstologNode left = visit(ctx.getChild(0));
                EstologNode right = visit(ctx.getChild(2));
                switch (ctx.getChild(1).getText()) {
                    case "=":
                        return EstologNode.vordus(left, right);
                    case "VOI":
                        return EstologNode.voi(left, right);
                    case "JA":
                    case "NING":
                        return EstologNode.ja(left, right);
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            @Override
            public EstologNode visitKuiSiis(KuiSiisContext ctx) {
                EstologNode kui = visit(ctx.avaldis(0));
                EstologNode siis = visit(ctx.avaldis(1));
                EstologNode muidu = null;
                if (ctx.avaldis(2) != null) muidu = visit(ctx.avaldis(2));
                return EstologNode.kui(kui, siis, muidu);
            }

            // Lihtne käsitletakse BaseVisitor-is vaikimisi
        };

        return visitor.visit(tree);
    }
}
